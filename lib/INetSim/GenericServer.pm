# -*- perl -*-
#
# INetSim::GenericServer - Generic server base package
#
# (c)2007-2019 Thomas Hungenberg, Matthias Eckert
#
#############################################################

package INetSim::GenericServer;

use base qw(INetSim::Fork);

# no shared functions

1;
#
