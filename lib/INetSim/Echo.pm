# -*- perl -*-
#
# INetSim::Echo - Base package for Echo::TCP and Echo::UDP
#
# (c)2007-2019 Thomas Hungenberg, Matthias Eckert
#
#############################################################

package INetSim::Echo;

use strict;
use warnings;
use base qw(INetSim::GenericServer);

# no shared functions

1;
#
